﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
//using SistemaDeMatricula.Migrations;
using SistemaDeMatricula.Modelos;

namespace SistemaDeMatricula
{
    class Menu
    {
        public void Login()
        {
            string correoIngresado = "";
            string contraseñaIngresada = "";
            Console.WriteLine();
            Console.WriteLine("1.-Estudiate   2.-Administrador");
            int opcion = int.Parse(Console.ReadLine());
            if (opcion == 1)
            {
                Console.WriteLine("===Iniciar Sesión===");
                Console.Write("usuario: ");
                correoIngresado = Console.ReadLine();
                Console.Write("cotraseña: ");
                contraseñaIngresada = Console.ReadLine();

               using (var db = new SistemaContext())
               {
                    var estudiante = (from resultado in db.estudiantes
                                     where resultado.Correo == correoIngresado
                                     select resultado).FirstOrDefault<Estudiante>();
                    if (estudiante.Contraseña == null)
                        Console.WriteLine("no existe estudiante");
                    if (estudiante.Contraseña == contraseñaIngresada)
                    {

                        //mostrar menú con opciones del estudiante como ver información personal y sobre estado legal 
                        Console.WriteLine("Estudiantes");
                    }
                    else
                    {
                        Console.WriteLine("usuario o cotraseña inválidas");
                        Login();
                    }
               }

            }
            else
            {
                if (opcion == 2)
                {
                    Console.WriteLine("===Iniciar Sesión===");
                    Console.Write("usuario:");
                    correoIngresado = Console.ReadLine();
                    Console.Write("cotraseña");
                    contraseñaIngresada = Console.ReadLine();

                    using (var db = new SistemaContext())
                    {
                        var administrador = (from resultado in db.administradors
                                          where resultado.Correo == correoIngresado
                                          select resultado).FirstOrDefault<Administrador>();
                        if (administrador.Contraseña == contraseñaIngresada)
                        {
                            MenuAdministrador();
                            //mostrar menú con opciones del administrador como ingresar estudiantes al sistema, gestionar documento y realizar reportes 
                            Console.WriteLine("administrador");
                        }
                        else
                        {
                            Console.WriteLine("usuario o cotraseña inválidas");
                            Login();
                        }
                    }

                }
            }
           
        }
        public void MenuAdministrador()
        {
            InterfazAdministrador InterfazAdmin = new InterfazAdministrador();
            AdminInfo();
            Console.WriteLine("1.- Ingresar o modificar Estudiante \n2.-Registrar documentos del estudiante" +
                "\n3.- Visualizar reportes  \n4.-salir");
            int opcion = int.Parse(Console.ReadLine());
            switch (opcion)
            {
                case 1:
                    InterfazAdmin.RegistroEstudiante();
                    MenuAdministrador();
                    break;

                case 2:
                    InterfazAdmin.RegistroDocumento();
                    MenuAdministrador();
                    break;
                case 3:
                    InterfazAdmin.RegistroFicha();
                    MenuAdministrador();
                    break;
                case 4:
                    Login();
                    break;
                 default:

                    MenuAdministrador();
                    break;
            }
            //

        }
        public void AdminInfo()
        {
            using (var db = new SistemaContext())
            {
                List<Administrador> ListAdministrador = db.administradors.ToList();
                foreach (var admin in ListAdministrador)
                {
                    Console.WriteLine(admin.Nombre+""+ admin.Apellido + " \n" + admin.Cedula );
                }
            }
            return;

        }
    }
}
