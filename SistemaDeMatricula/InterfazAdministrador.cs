﻿using System;
using System.Collections.Generic;
using System.Text;
using SistemaDeMatricula.Modelos;

namespace SistemaDeMatricula
{
    class InterfazAdministrador
    {
        public void RegistroEstudiante()
        {
            using (var db = new SistemaContext())
            {
                Estudiante estudiante = Estudiante.Estudiantes;
                Console.Write("nombres: ");
                estudiante.Nombre = Console.ReadLine();
                Console.Write("apellidos: ");
                estudiante.Apellido = Console.ReadLine();
                Console.Write("numero de cédula: ");
                estudiante.Cedula = Console.ReadLine();
                Console.Write("carrera: ");
                estudiante.Carrera = Console.ReadLine();
                Console.Write("nivel (escriba el número): ");
                estudiante.IdNivel = int.Parse(Console.ReadLine());
                Console.Write("correo: ");
                estudiante.Correo = Console.ReadLine();
                Console.Write("contraseña: ");
                estudiante.Contraseña = Console.ReadLine();

                db.Add(estudiante);
                db.SaveChanges();
            }
            return;
        }
        public void RegistroDocumento()
        {

        }
        public void RegistroFicha()
        {

        }
        public void Reporte()
        {

        }

    }
}
