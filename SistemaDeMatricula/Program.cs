﻿using System;
using SistemaDeMatricula.Modelos;

using System.Linq;
using System.Collections.Generic;

namespace SistemaDeMatricula
{
    class Program
    {
        static void Main(string[] args)
        {
            Menu menu = new Menu();
            InterfazAdministrador interfazAdministrador = new InterfazAdministrador();
            Console.WriteLine("Hello World!");
            //menu.Login();
            interfazAdministrador.RegistroEstudiante();

        }
        static void Listado()
        {
            using (var db = new SistemaContext())
            {
                List<Estudiante> ListEstudiantes = db.estudiantes.ToList();
                foreach (var student in ListEstudiantes)
                {
                    Console.WriteLine(student.Apellido + "" + student.Cedula + " " + student.Carrera);
                }
            }
            return;
        }


        static void IngresarEstudiante()
        {
            using (var db = new SistemaContext())
            {
                Estudiante estudiante = Estudiante.Estudiantes;
                Console.WriteLine("nombres");
                estudiante.Nombre = Console.ReadLine();
                Console.WriteLine("apellidos");
                estudiante.Apellido = Console.ReadLine();
                Console.WriteLine("numero de cédula");
                estudiante.Cedula = Console.ReadLine();
                Console.WriteLine("carrera");
                estudiante.Carrera = Console.ReadLine();
                Console.WriteLine("nivel (escriba el número)");
                estudiante.IdNivel = int.Parse(Console.ReadLine());
                Console.WriteLine("correo");
                estudiante.Correo = Console.ReadLine();
                Console.WriteLine("contraseña");
                estudiante.Contraseña = Console.ReadLine();

                db.Add(estudiante);
                db.SaveChanges();
            }
            return;
        }
        static void IngresarDocumento()
        {
            using (var db = new SistemaContext())
            {
                Documento document = Documento.Documentos;
                Console.WriteLine("ingrese el nombre del documento que desea ingresar");
                document.NombreDocumento = Console.ReadLine();
                db.Add(document);
                db.SaveChanges();
            }
            return;
        }
        static void Eliminar()
        {
            using (var db = new SistemaContext())
            {
                Estudiante estudiante = db.estudiantes.Find(1);
                db.estudiantes.Remove(estudiante);
                db.SaveChanges();
            }
            return;
        }
        static void EliminarDocumento()
        {
            using (var db = new SistemaContext())
            {
                Documento document = db.documentos.Find(1);
                db.documentos.Remove(document);
                db.SaveChanges();
            }
            return;
        }
        static void Modificar()
        {
            using (var db = new SistemaContext())
            {
                Estudiante estudiante = db.estudiantes.Find();
                estudiante.Carrera = "";
                db.SaveChanges();
            }
        }
        static void Metodo()
        {
            using (var db = new SistemaContext())
            {
                var documento = (from resultado in db.documentos
                                 where resultado.NombreDocumento == "cedula"
                                 select resultado).ToList<Documento>();
                foreach (var item in documento)
                {
                    db.documentos.Remove(item);
                    db.SaveChanges();
                }
            }

        }


    }
}
