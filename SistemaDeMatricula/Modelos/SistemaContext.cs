﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace SistemaDeMatricula.Modelos
{
    class SistemaContext : DbContext
    {
        private const string connectionString =
    "Server=.\\SQLEXPRESS; Database=Sistema; Trusted_Connection=true;";
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)

        {
            optionsBuilder.UseSqlServer(connectionString);

        }
        //Server=localhost\SQLEXPRESS02;Database=master;Trusted_Connection=True;
        public DbSet<Documento> documentos { get; set; }
        public DbSet<Administrador> administradors { get; set; }
        public DbSet<Estudiante> estudiantes { get; set; }
        public DbSet<Nivel> nivels { get; set; }
        public DbSet<Reportes> reportes { get; set; }
    }
}
