﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SistemaDeMatricula.Modelos
{
    class Estudiante
    {
        private static Estudiante estudiante;

        protected Estudiante()
        {

        }

        public static Estudiante Estudiantes
        {
            get
            {
                if (estudiante == null)
                {
                    estudiante = new Estudiante();
                }
                return estudiante;
            }
        }

        public int Id { get; set; }
        public string Carrera { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Cedula { get; set; }
        public int IdNivel { get; set; }
        public string Correo { get; set; }
        public string Contraseña { get; set; }

    }
}
