﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SistemaDeMatricula.Modelos
{
    class Administrador
    {
        private static Administrador administrador;

        protected Administrador()
        {

        }

        public static Administrador Administradors
        {
            get
            {
                if (administrador == null)
                {
                    administrador = new Administrador();
                }
                return administrador;
            }
        }

        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Cedula { get; set; }
        public string Correo { get; set; }
        public string Contraseña { get; set; }

    }
}
