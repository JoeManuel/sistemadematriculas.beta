﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SistemaDeMatricula.Modelos
{
    class Reportes
    {
        private static Reportes reporte;

        protected Reportes()
        {

        }

        public static Reportes Reporte
        {
            get
            {
                if (reporte == null)
                {
                    reporte = new Reportes();
                }
                return reporte;
            }
        }
        public int Id { get; set; }
        public int IdEstudiantes { get; set; }
        public int IdDocumento { get; set; }
        public string Estado { get; set; }
    }
}
